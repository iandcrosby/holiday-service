import axios, { AxiosResponse } from 'axios';
import bottleneck from 'bottleneck';
import cacheLocal from '../../utils/cache_local';

const NAGER_URL = process.env.NAGER_URL || `https://date.nager.at/api`;
export class HolidayService {
    public async holiday(date: string): Promise<any> {

        const hols: any[] | undefined | null = cacheLocal.get<any[]>(date);
        if (!hols) {
            console.log("Cache miss.");

            const holidays = await getHolidaysByDate(date);
            // First fetch all country codes
            if (!holidays) {
                throw new Error(`Holidays not found`);
            }
          cacheLocal.set(date, holidays);
          return holidays;
        }
        console.log("Cache hit.");
        return hols;
    }
}

async function getHolidaysByDate(date: string) {
    const limiter = new bottleneck({
        maxConcurrent: 10
      });
    const codes = await getCountryCodes();
    const req: any[] = [];
    codes.forEach(async code => {
        getHolidaysByCountry(code, date);
        req.push(getHolidaysByCountry(code, date));
    });
    const allHolidays: Record<string,any> = {};
    try {
        // TODO - allSettled may be preferred?
        const res = await limiter.schedule(() => Promise.all(req));

        // TODO fix below
        res.filter(function (e: any[]) { return e.length > 0;}).map(function(item: { countryCode: string; name: string; }[]) {
            // TODO handle case where multiple holidays on same day
            const code = item[0].countryCode;
            allHolidays[code] = item[0].name;
        });
    } catch (error) {
        console.log(error);
    }
    console.log(allHolidays);
    return allHolidays;
}

// Fetch all country codes
async function getCountryCodes(): Promise<string[]> {
    const result: AxiosResponse = await axios.get(NAGER_URL + `/v3/AvailableCountries`);
    const data = result.data.map(function(a: { countryCode: string; name: string; }) { return a.countryCode; });
    return data;
}

// Fetch all holidays for sepcified country for specified date
async function getHolidaysByCountry(countryCode: string, date: string) {
    const year = date.substring(0,4);
    // TODO template the URL
    const result: AxiosResponse = await axios.get(NAGER_URL + `/v3/PublicHolidays/` + year + `/` + countryCode);
    const data = result.data.filter((hol: { date: string; }) => hol.date === date);
    return data;
}