import { HolidayService } from '@service/holiday-service';
import express from 'express';
import { Inject } from 'typescript-ioc';

export class HolidayController {

    private holidayService: HolidayService;

    constructor(@Inject holidayService: HolidayService) {

        this.holidayService = holidayService;
    }

    public async get(req: express.Request, res: express.Response): Promise<void> {

        if (isDateValid(req.params.date)) {
            const holidays = await this.holidayService.holiday(req.params.date as string);
            res.status(200).json(holidays);
        } else {
            res.status(400).json({"error_msg": "Invalid date format"});
        }
    }
}

// Validate date parameter is correct format
//   and that the date is not before today 
//   and not more than 364 days in the future
function isDateValid(date: string) {
    const regEx = /^\d{4}-\d{2}-\d{2}$/;
    
    if (!date.match(regEx)) {
        return false;
    }
    const d = new Date(date);
    const time = d.getTime();
    if(!time && time !== 0) {
        return false;
    }
    const today = new Date();
    today.setHours(0,0,0,0);
    const diffTime = d.getTime() - today.getTime();
    // divide by # of milliseconds per day
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    if (diffDays < 0 || diffDays > 364) {
        return false;
    }

    return d.toISOString().slice(0,10) === date;
  }