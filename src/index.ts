import 'module-alias/register';
import express from 'express';
import { holidayRouter } from './controllers/holidays';

const app = express();
const port = process.env.PORT || 8080; // default port to listen

app.use('/api', holidayRouter);

app.listen(port, () => {
    console.log(`Server started at http://localhost:${port}`);
});