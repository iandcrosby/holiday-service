import express from 'express';
import { Container } from 'typescript-ioc';
import { HolidayController } from './holiday-controller';

const urlPath = '/holiday/:date';

const router = express.Router();

const holidayController = Container.get(HolidayController);

router.get(urlPath, (req, res) => {
    holidayController.get(req, res);
});

export { router as holidayRouter };