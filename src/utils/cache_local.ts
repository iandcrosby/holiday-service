import NodeCache from 'node-cache';

type Key = string
class CacheLocal {
  private static _instance: CacheLocal

  private cache: NodeCache

  private constructor(ttlSeconds: number) {
    this.cache = new NodeCache({
      stdTTL: ttlSeconds,
      checkperiod: ttlSeconds * 0.2,
      useClones: false
    });
  }
  
  public static getInstance(): CacheLocal {
    if (!CacheLocal._instance) {
        // TTL default to 10 minutes
      const cacheTTL = Number(process.env.CACHE_TTL) || 6000;
      CacheLocal._instance = new CacheLocal(cacheTTL);
    }
    return CacheLocal._instance;
  }
  
  public get<T>(key: Key): T | undefined {
    return this.cache.get<T>(key);
  }

  public set<T>(key: Key, value: T): void {
    this.cache.set(key, value);
  }
}

export default CacheLocal.getInstance();