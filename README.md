# Holiday Finder

A web service that stores and returns all holidays (worldwide) for the next year.

## Design Decisions

1. To solve this problem practically, I would fetch the data on startup and build an internal store from which we could satisfy the API requests. The current implementation makes redundant API calls for each incoming request. It is assumed this is not the nature of the requirement based on: "Please do not try to scrape the entire dataset from the public API as your solution."

2. I have used in-memory cache (node-cache) - in a wider system, I would leverage an external cache (e.g. AWS Elasticache, Redis, etc)

3. Library vs write your own - in practice I would generally always use a library. But for the sake of the exercise I have occasionally avoided using a library.
  - Validating requests
  - Compare dates

4. Cache defaulted to 10 minute TTL. Can be configured via environment variable.

## TODO

- Handle promise rejections
- Tests
- Clean Code
- Doc
- Containerize
- Redis cache
- AWS setup
